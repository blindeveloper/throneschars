# Game of thrones characters app

[Live demo](https://throneschars.web.app/)
## Available Scripts

In the project directory, you can run:
### `yarn install`
do it once right after the cloning of app from bitbucket
### `yarn start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `yarn test`

Launches the test runner in the interactive watch mode.

### `yarn build`

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified, and the filenames include the hashes.
Your app is ready to be deployed!

## Technical details
**Core of this App**: Decided to go with create-react-app builder for saving time on tuning all in app configurations.

**Design**: Chose Material UI mostly for grid and basic components use. I like it for prototyping some ideas + it saves tons of time.

**Data sharing**: Was considering between props, redux and context. Decided to use props with an idea to switch to the context in the end but haven't time for this improvement

**CI/CD**: I configured CI/CD with Bitbucket Pipelines and Firebase. It has 2 modes, if push was made not in master branch, then it will run unit tests to become green/red. But if push was made in master then regardless tests, it will make a build ready code and will deploy it to firebase. Funny moment was when I met building time limit(50min/month) of bitbucket-pipelines, now I have extended plan =) 

**Caching**: I tried to make this app fully covered with cache idea. This app should not request any data again if they were previously requested. + I tried to store to the cache only useful info

**Routing**: As experiment part, I decided to use hookrouter instead of famous react-router. Happy with results, this lib is easy to use, and it's based on hooks which I like a lot in react

## What would I improve if I would have more time
**Data sharing** between react components via hooks or redux instead of props 

**Styles**: better styles files/scss organization. In the end I am using single css file for all styles, Ideally is to have style per component. It would add much more flexibility.

**Tests**: I would write much more test + some e2e with cypress

**Typescript**: I was planning to use it, but just because of time limits decided to jump without it.

**Better fetch interceptors**: for catching all requests in one place. Same for errors.

**Search/filter component** by name: I was missing this component in this app