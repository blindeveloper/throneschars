import { getCacheByKey, setCacheValue } from '../services/localStorageManager'
import { CACHE, API, PAGE_SIZE } from '../services/consts'
import { fetchDataByUrl, fetchAll } from '../services/resource'

export const getCharacterIdByUrl = url => url && url.split('/characters/')[1]
export const getCharacterUrlById = id => `${API.BASE_URL}${API.CHARACTERS}${id}`

export const getBookIdByUrl = url => url && url.split('/books/')[1]
export const getBookUrlById = id => `${API.BASE_URL}${API.BOOKS}${id}`

export const getHouseIdByUrl = url => url && url.split('/houses/')[1]
export const getHouseUrlById = id => `${API.BASE_URL}${API.HOUSES}${id}`

export const getFilteredCharactersByBookUrl = (charactersList, selectedBookUrl) => {
  return charactersList.filter(character => character.books.includes(selectedBookUrl))
}

export const getCharactersPicture = (characterId, gender, handleSetIsShowProgressBar, cb) => {
  const cachedPictures = getCacheByKey(CACHE.PICTURES)
  if (cachedPictures && cachedPictures[characterId]) {
    cb(cachedPictures[characterId])
  } else {
    fetchDataByUrl(`${API.RANDOMUSER}?gender=${gender}&seed=${characterId}&inc=picture`, handleSetIsShowProgressBar)
      .then(pictureInfo => {
        const picture = pictureInfo && 
          pictureInfo.results && 
          pictureInfo.results[0] && 
          pictureInfo.results[0].picture
          
        if (picture) {
          setCacheValue(CACHE.PICTURES, {
            ...cachedPictures,
            [characterId]:picture
          })
          cb(picture)
        } else {
          cb(null)
        }
      })
  }
}

export const getCharacterById = (characterId, handleSetIsShowProgressBar, cb) => {
  const cacheByKey = getCacheByKey(CACHE.CHARACTERS)
  if (cacheByKey && cacheByKey[characterId]) {
    cb(cacheByKey[characterId])
  } else {
    fetchDataByUrl(`${API.BASE_URL}${API.CHARACTERS}${characterId}`, handleSetIsShowProgressBar)
    .then(loadedCharacter => {
      cb(loadedCharacter)
    })
  }
}

export const getAllegiancesList = characters => {
  let allegiances = []
  characters.forEach(char => {
    if(char.allegiances.length) {
      allegiances = [...allegiances, ...char.allegiances]
    }
  })
  return [...new Set(allegiances)]//set will return unique vals
}

export const getAllegiances = (urls, handleSetIsShowProgressBar, cb) => {
  const cachedAllegiances = getCacheByKey(CACHE.HOUSES)
  let allegiancesUrlsToRequest = urls
  
  if (cachedAllegiances) {
    allegiancesUrlsToRequest = urls.filter(url => !cachedAllegiances[getHouseIdByUrl(url)])
  }

  if(allegiancesUrlsToRequest.length) {
    fetchAll(allegiancesUrlsToRequest, handleSetIsShowProgressBar).then(res => {
      let allegiancesSet = {}
      res.forEach(el => allegiancesSet[getHouseIdByUrl(el.url)] = { name: el.name })
      const updatedAllegiances = {...cachedAllegiances, ...allegiancesSet}
      setCacheValue(CACHE.HOUSES, updatedAllegiances)
      cb(updatedAllegiances)
    })
  } else {
    cb(cachedAllegiances)
  }
}

export const getBooks = (handleSetIsShowProgressBar, cb) => {
  const cachedBooks = getCacheByKey('books')
  if (!cachedBooks) {
    fetchDataByUrl(`${API.BASE_URL}${API.BOOKS}?pageSize=20`, handleSetIsShowProgressBar).then(res => {
      let booksSet = {}
      res.forEach(el => booksSet[getBookIdByUrl(el.url)] = { url: el.url, name: el.name })
      setCacheValue('books', booksSet)
      cb(booksSet)
    })
  } else {
    cb(cachedBooks)
  }
}

export const getCharacters = (pageNumber, handleSetIsShowProgressBar, cb) => {
  const cachedCharacters = getCacheByKey('characters')
  const cachedPages = getCacheByKey('pages')

  if (cachedPages && cachedPages[pageNumber] && cachedPages[pageNumber].length) {
    cb(cachedPages[pageNumber].map(charId => cachedCharacters[charId]))
  } else {
    fetchDataByUrl(`${API.BASE_URL}${API.CHARACTERS}?page=${pageNumber}&pageSize=${PAGE_SIZE}`, handleSetIsShowProgressBar)
    .then(charResponse => {
      let charactersSet = {}
      charResponse.forEach(el => charactersSet[getCharacterIdByUrl(el.url)] = el)
      setCacheValue('characters', {...cachedCharacters, ...charactersSet})
      setCacheValue('pages', {
        ...cachedPages,
        [pageNumber]: charResponse.map(el => getCharacterIdByUrl(el.url))
      })
      cb(charResponse)
    })
  }
}

export const getCharactersByUrlList = (urlList, handleSetIsShowProgressBar, cb) => {
  let charactersByUrlList = {}
  const cachedCharacters = getCacheByKey('characters')
  let filteredUrlListForSearch = urlList
  if(cachedCharacters) {
    filteredUrlListForSearch = urlList.filter(url => cachedCharacters && !cachedCharacters[getCharacterIdByUrl(url)])
  
    urlList.forEach(url => {
      if(cachedCharacters && cachedCharacters[getCharacterIdByUrl(url)]) {
        charactersByUrlList[getCharacterIdByUrl(url)] = cachedCharacters[getCharacterIdByUrl(url)]
      }
    })
  }
  
  fetchAll(filteredUrlListForSearch, handleSetIsShowProgressBar).then(res => {
    setCacheValue('characters', {...cachedCharacters, ...res})
    res.forEach(el => charactersByUrlList[getCharacterIdByUrl(el.url)] = el)
    cb(charactersByUrlList)
  })
}

export const getCharacterRelativesUrls = character => {
  let relativesUrls = []

  character.father && relativesUrls.push(character.father)
  character.mother && relativesUrls.push(character.mother)

  if (Array.isArray(character.spouse) && character.spouse.length) {
    relativesUrls = [...relativesUrls, ...character.spouse]
  } else {
    character.spouse && relativesUrls.push(character.spouse)
  }

  return relativesUrls
}

export const getComma = (index, list) => {
  return (index !== list.length - 1) ? `${ ', '}` : null
}