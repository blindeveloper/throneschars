export const API = {
  RANDOMUSER: 'https://randomuser.me/api/',
  BASE_URL: 'https://www.anapioficeandfire.com/api/',
  CHARACTERS: 'characters/',
  BOOKS: 'books/',
  HOUSES: 'houses/'
}

export const PAGE_SIZE = 20

export const NAVIGATION = {
  HOME: '/',
  CHARACTER: '/character/'
}

export const CACHE = {
  CHARACTERS: 'characters',
  BOOKS: 'books',
  ALLEGIANCES: 'allegiances',
  HOUSES: 'houses',
  PICTURES: 'pictures',
}