export async function fetchDataByUrl(url, handleSetIsShowProgressBar) {
  try {
    handleSetIsShowProgressBar(true)
    const response = await fetch(url, {
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json'
      },
      referrerPolicy: 'no-referrer'
    });
    handleSetIsShowProgressBar(false)
    return response.json();
  } catch(err) {
    handleSetIsShowProgressBar(false)
    throw new Error(err)
  }
}

export const fetchAll = async (urls, handleSetIsShowProgressBar) => {
  try {
    handleSetIsShowProgressBar(true)
    const res = await Promise.all(urls.map(u => fetch(u)))
    handleSetIsShowProgressBar(false)
    return await Promise.all(res.map(r => r.json()))
  } catch(err) {
    handleSetIsShowProgressBar(false)
    throw new Error(err)
  }
}