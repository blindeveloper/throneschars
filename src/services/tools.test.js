import { 
  getCharacterIdByUrl, 
  getCharacterUrlById,
  getBookIdByUrl,
  getBookUrlById,
  getHouseIdByUrl,
  getHouseUrlById,
  getAllegiancesList,
  getCharacterRelativesUrls,
  getComma
} from './tools'

describe('---tools services test---', () => {  
  it('should test getCharacterIdByUrl function', () => {
    expect(getCharacterIdByUrl('https://www.anapioficeandfire.com/api/characters/836')).toEqual("836")
  })

  it('should test getCharacterUrlById function', () => {
    expect(getCharacterUrlById('836')).toEqual("https://www.anapioficeandfire.com/api/characters/836")
  })

  it('should test getBookIdByUrl function', () => {
    expect(getBookIdByUrl('https://www.anapioficeandfire.com/api/books/34')).toEqual("34")
  })
  
  it('should test getBookUrlById function', () => {
    expect(getBookUrlById('34')).toEqual("https://www.anapioficeandfire.com/api/books/34")
  })
  
  it('should test getHouseIdByUrl function', () => {
    expect(getHouseIdByUrl('https://www.anapioficeandfire.com/api/houses/434')).toEqual("434")
  })

  it('should test getHouseUrlById function', () => {
    expect(getHouseUrlById('434')).toEqual("https://www.anapioficeandfire.com/api/houses/434")
  })
  
  it('should test getAllegiancesList function', () => {
    expect(getAllegiancesList([
      { allegiances: ['https://www.anapioficeandfire.com/api/houses/169', 'https://www.anapioficeandfire.com/api/houses/178']},
      { allegiances: ['https://www.anapioficeandfire.com/api/houses/162', 'https://www.anapioficeandfire.com/api/houses/178']},
    ])).toEqual([
      "https://www.anapioficeandfire.com/api/houses/169", 
      "https://www.anapioficeandfire.com/api/houses/178", 
      "https://www.anapioficeandfire.com/api/houses/162"
    ])
  })

  it('should test getCharacterRelativesUrls function with spouse as single string', () => {
    expect(getCharacterRelativesUrls({
      father: 'https://www.anapioficeandfire.com/api/characters/12',
      mother: 'https://www.anapioficeandfire.com/api/characters/34',
      spouse: 'https://www.anapioficeandfire.com/api/characters/67'
    })).toEqual([
      'https://www.anapioficeandfire.com/api/characters/12',
      'https://www.anapioficeandfire.com/api/characters/34',
      'https://www.anapioficeandfire.com/api/characters/67'
    ])
  })

  it('should test getCharacterRelativesUrls function with spouses as list of strings', () => {
    expect(getCharacterRelativesUrls({
      father: 'https://www.anapioficeandfire.com/api/characters/12',
      mother: 'https://www.anapioficeandfire.com/api/characters/34',
      spouse: ['https://www.anapioficeandfire.com/api/characters/67', 'https://www.anapioficeandfire.com/api/characters/31']
    })).toEqual([
      'https://www.anapioficeandfire.com/api/characters/12',
      'https://www.anapioficeandfire.com/api/characters/34',
      'https://www.anapioficeandfire.com/api/characters/67',
      'https://www.anapioficeandfire.com/api/characters/31'
    ])
  })
  
  it('should test getComma function', () => {
    expect(getComma(2, [1,2,3])).toEqual(null)
    expect(getComma(2, [1,2,3,4,5])).toEqual(", ")
  })
})