export const getAllCache = () => {
  let result = {}
  const storage = { ...window.localStorage }
  for (const property in storage) {
    result[property] = JSON.parse(storage[property])
  }
  return result
}

export const getCacheByKey = key => {
  return JSON.parse(window.localStorage.getItem(key))
}

export const setCacheValue = (key, value, cb) => {
  window.localStorage.setItem( key, JSON.stringify(value) )
  cb && cb()
}
