import React, { useState, useEffect } from 'react'
import { getBooks } from './services/tools'
import {useRoutes} from 'hookrouter';
import Characters from './components/characters/characters'
import CharacterDetails from './components/character-details/characterDetails'
import PageNotFound from './components/page-not-found/pageNotFound'
import ProgressBar from './components/progress-bar/progressBar'
import './index.css'
import Snowfall from 'react-snowfall'
import { Grid } from '@mui/material';


const App = () => {
  const [ charactersList, setCharactersList ] = useState(null)
  const [ books, setBooks ] = useState(null)
  const [ houses, setHouses ] = useState(null)
  const [ isShowProgressBar, setIsShowProgressBar ] = useState(false)

  const routes = {
    '/': () => <Characters 
      setHousesHandler={setHouses} 
      setCharactersListHandler={setCharactersList} 
      charactersList={charactersList} 
      books={books} 
      houses={houses}
      handleSetIsShowProgressBar={setIsShowProgressBar}/>,
    '/character/:id':({id})=> <CharacterDetails 
      characterId={id}
      books={books}
      houses={houses} 
      handleSetIsShowProgressBar={setIsShowProgressBar} 
    />
  };

  useEffect(() => {
    getBooks(setIsShowProgressBar, booksList => setBooks(booksList))
  }, [])

  const routeResult = useRoutes(routes)

  return <div className="root-wrapper">
    <Snowfall style={{position: 'fixed', top: 0}}/>
    {isShowProgressBar && <ProgressBar/>}
    <Grid container spacing={2} justifyContent="center">
      <Grid item xs={12} sm={12} md={12} lg={8} xl={6}>
        {routeResult || <PageNotFound/>}
      </Grid>
    </Grid>
  </div>
}

export default App;
