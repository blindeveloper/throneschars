import React, { useEffect, useState } from 'react'
import { 
  getCharacterIdByUrl, 
  getCharactersPicture 
} from '../../services/tools'

const CharactersPicture = props => {
  const { character, handleSetIsShowProgressBar } = props
  const [ charactersPicture, setCharactersPicture ] = useState()
  useEffect(() => {
    getCharactersPicture(getCharacterIdByUrl(character.url), 
      character.gender.toLowerCase(),
      handleSetIsShowProgressBar,
      picture => setCharactersPicture(picture))
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])
  return <>
    {
      charactersPicture && <img className="avatar" src={charactersPicture.large} alt="characters face"/>
    }
  </>
}

export default CharactersPicture