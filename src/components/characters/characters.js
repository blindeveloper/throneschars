import React, { useState, useEffect, useRef } from 'react'
import useOnScreenHook from '../../useOnScreenHook'
import List from '../list/list'

import { 
  getFilteredCharactersByBookUrl, 
  getAllegiancesList, 
  getAllegiances,
  getCharacters
} from '../../services/tools'

const Characters = props => {
  const { 
    handleSetIsShowProgressBar,
    setCharactersListHandler, 
    setHousesHandler, 
    charactersList, 
    houses,
    books
  } = props

  const [ currentPageNumber, setCurrentPageNumber ] = useState(1)
  const [ selectedBookUrl, setSelectedBookUrl ] = useState('')
  const bottomOfScreen = useRef();
  const isBottomOfScreen = useOnScreenHook(bottomOfScreen)

  useEffect(() => {
    if (isBottomOfScreen) {
      setCurrentPageNumber(currentPageNumber + 1)
      updateCharacters()
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isBottomOfScreen])

  const updateCharacters = () => {
    getCharacters(currentPageNumber, handleSetIsShowProgressBar, currentPageCharacters => {
      getAllegiances(
        getAllegiancesList(currentPageCharacters), handleSetIsShowProgressBar, 
        allegiances => setHousesHandler(allegiances)
      )
      let joinedCharacters = charactersList ? [...charactersList, ...currentPageCharacters, ] : currentPageCharacters
      if(selectedBookUrl) {
        joinedCharacters = getFilteredCharactersByBookUrl(joinedCharacters, selectedBookUrl)
      }

      setCharactersListHandler(joinedCharacters)
    })
  }

  const handleFilterByBook = selectedBook => {
    if(selectedBook.url === selectedBookUrl) {
      setSelectedBookUrl('')
      updateCharacters()
    } else {
      const filteredCharactersList = getFilteredCharactersByBookUrl(charactersList, selectedBook.url)
      setCharactersListHandler(filteredCharactersList)
      setSelectedBookUrl(selectedBook.url)
    }
  }

  return <React.Fragment>
    <h2 className="page-title">Game of thrones characters</h2>
    { charactersList && <List 
      listItems={charactersList} 
      books={books} 
      houses={houses} 
      handleFilterByBook={handleFilterByBook}
      selectedBookUrl={selectedBookUrl}/> }
    <div ref={bottomOfScreen}>footer</div>
  </React.Fragment>
}

export default Characters