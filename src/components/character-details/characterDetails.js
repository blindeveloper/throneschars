import React, {useEffect, useState} from 'react'
import { getCharacterById } from '../../services/tools'
import BasicCharacterInfo from '../basic-character-info/basicCharacterInfo'
import ExtendedCharacterInfo from '../extended-character-info/extendedCharacterInfo'
import { CardContent, Grid, Card } from '@mui/material';
import CharactersPicture from '../characters-picture/charactersPicture'
import BackButton from '../back-button/backButton'

const CharacterDetails = props => {
  const { characterId, books, houses, handleSetIsShowProgressBar } = props
  useEffect(() => {
    getCharacterById(characterId, handleSetIsShowProgressBar, char => {
      setSelectedCharacter(char)
    })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])
  const [ selectedCharacter, setSelectedCharacter ] = useState(null)
  return <>
    {
      selectedCharacter && <Card className="list-item character-item">
      <CardContent>
        <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
          <Grid item>
          <CharactersPicture
                character={selectedCharacter}
                handleSetIsShowProgressBar={handleSetIsShowProgressBar}/>  
          </Grid>
          <Grid item xs={8}>
          <BasicCharacterInfo
                character={selectedCharacter}
                books={books}
                houses={houses}
              />
              <ExtendedCharacterInfo 
                handleSetIsShowProgressBar={handleSetIsShowProgressBar} 
                character={selectedCharacter}/>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
    }
  <BackButton/>
  </>
}

export default CharacterDetails