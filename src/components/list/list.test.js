import { render } from '@testing-library/react'
import List from './list'

const props = {
  listItems: [
    {
      name: 'John'
    },
    {
      name: 'Sarah'
    }
  ]
}

describe('---List Component test---', () => {
  
  it('should render 2 list items', () => {
    const renderedComponent = render(<List {...props} />)
    const { container } = renderedComponent
    expect(container.querySelectorAll('.list-item').length).toEqual(2)
  })
  
  it('should have first name John', () => {
    const renderedComponent = render(<List {...props} />)
    const { container } = renderedComponent
    expect(container.querySelectorAll('.list-item .list-item-name')[0].innerHTML).toEqual('John')
  })
})