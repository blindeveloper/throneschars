import React from 'react'
import Card from '@mui/material/Card';
import { CardContent, Grid } from '@mui/material';
import { A } from 'hookrouter';
import { getCharacterIdByUrl } from '../../services/tools'
import BasicCharacterInfo from '../basic-character-info/basicCharacterInfo'

const List = props => {
  const { 
    handleFilterByBook,
    selectedBookUrl,
    listItems,
    houses,
    books
  } = props

  return <Grid container spacing={2} className="list-wrapper">
    {
      listItems.map((character, index) => {
        return <Grid item xs={12} sm={6} md={4} lg={4} xl={3} className="best-customer" key={index}>
          <A className='Link' href={`/character/${getCharacterIdByUrl(character.url)}`}>
            <Card className="list-item character-item">
              <CardContent>
                <BasicCharacterInfo
                  character={character}
                  books={books}
                  selectedBookUrl={selectedBookUrl}
                  handleFilterByBook={handleFilterByBook}
                  houses={houses}
                />
              </CardContent>
            </Card>
          </A>
        </Grid>
        })  
    }
  </Grid>
}

export default List