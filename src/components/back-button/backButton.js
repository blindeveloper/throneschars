import { Button } from '@mui/material';

const BackButton = () => {
  return <Button 
    variant="text" 
    className="back-button" 
    onClick={() => window.history.back()}>{'< back'}</Button>
}

export default BackButton