
import React, { useEffect, useState } from 'react'
import Typography from '@mui/material/Typography';
import { 
  getCharactersByUrlList, 
  getCharacterRelativesUrls, 
  getCharacterIdByUrl
} from '../../services/tools'

const ExtendedCharacterInfo = props => {
  const { character, handleSetIsShowProgressBar } = props
  const [ relatives, setRelatives ] = useState()
  
  useEffect(() => {
    const relativesUrls = getCharacterRelativesUrls(character)
    relativesUrls && relativesUrls.length && 
      getCharactersByUrlList(relativesUrls, handleSetIsShowProgressBar, chars => {
        setRelatives(chars)
      })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return <>
    {
      character.gender && <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
        Gender: <span data-testid="gender">{character.gender}</span>
      </Typography>
    }
    {
      character.culture && <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
        Culture: <span data-testid="culture">{character.culture}</span>
      </Typography>
    }
    {
      character.father && <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
        <span data-testid="father">Father: {relatives && relatives[getCharacterIdByUrl(character.father)].name}</span>
      </Typography>
    }
    {
      character.mother && <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
        Mother: {relatives && relatives[getCharacterIdByUrl(character.mother)].name}
      </Typography>
    }
    
    {
      character.titles && character.titles.filter(el => el !== '').length ? <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
        Titles: {character.titles.map((title, index) => {
          return <span key={index}>{title} </span>
        })}
      </Typography> : null
    }

    {
      character.spouse && (Array.isArray(character.spouse) ? 
      <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
        Spouses: {
          character.spouse.map(spouseUrl => {
            return relatives && relatives[getCharacterIdByUrl(spouseUrl)].name
          })
        }
      </Typography> : 
      <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
        Spouse: {relatives && relatives[getCharacterIdByUrl(character.spouse)].name}
      </Typography>)
    }
  </>
}

export default ExtendedCharacterInfo