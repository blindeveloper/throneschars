import { render } from '@testing-library/react'
import ExtendedCharacterInfo from './extendedCharacterInfo'

const props = {
  character: {
    gender: 'Female',
    culture: 'Ironborn',
    father: 'https://www.anapioficeandfire.com/api/characters/836',
    mather: 'https://www.anapioficeandfire.com/api/characters/66',
    titles: [],
    spouses: [],
  },
  handleSetIsShowProgressBar: () => {}
}

describe('---ExtendedCharacterInfo Component test---', () => {  
  it('should check correct data fields', () => {
    const { getByTestId } = render(<ExtendedCharacterInfo {...props} />)
    expect(getByTestId("gender").innerHTML).toEqual("Female")
    expect(getByTestId("culture").innerHTML).toEqual("Ironborn")
  })
})