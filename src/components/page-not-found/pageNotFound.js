import { CardContent, Card } from '@mui/material'
import BackButton from '../back-button/backButton'

const PageNotFound = () => {
  return <div>
    <Card className="list-item character-item">
      <CardContent>
        <h3>404 page not found</h3>
        <p>We are sorry but the page you are looking for does not exist.</p>
      </CardContent>
    </Card>
    <BackButton/>
  </div>
}

export default PageNotFound