import React from "react"
import Typography from '@mui/material/Typography';
import { getBookIdByUrl, getHouseIdByUrl, getComma } from '../../services/tools'

const BasicCharacterInfo = props => {
  const { character, books, selectedBookUrl, handleFilterByBook, houses } = props
  const selectBook = (e, bookUrl) => {
    if (handleFilterByBook) {
      e.stopPropagation()
      e.preventDefault()
      handleFilterByBook(books[getBookIdByUrl(bookUrl)])
    }
  }

  return <React.Fragment>
    <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
      <span className="list-item-name" data-testid="list-item-name">{ character.name || 'Unknown character' }</span>
    </Typography>
    {
      character.allegiances && character.allegiances.length && houses ? 
      <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
        <span data-testid="alliances">With alliances to: { character.allegiances.map((houseUrl, index) => {
          return <span key={index}>
            {houses[getHouseIdByUrl(houseUrl)] && houses[getHouseIdByUrl(houseUrl)].name}
            {getComma(index, character.allegiances)}
          </span>
        }) }</span>
      </Typography> : null
    }
    <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
      <span data-testid="was-born">{ character.born && `was born ${character.born}`}</span>
      <span data-testid="died">{ character.died && `died: ${character.died}` }</span>
    </Typography>
    {
      character.books && character.books.length && books ? 
      <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
        <span data-testid="meet-in-books">Meet in books: { character.books.map((bookUrl, index) => {
          return <span 
          className={`book-item ${selectedBookUrl === bookUrl && 'selected-book'}`}
          key={index} 
          onClick={e => selectBook(e, bookUrl)}>
            {books[getBookIdByUrl(bookUrl)] && books[getBookIdByUrl(bookUrl)].name}
            {getComma(index, character.books)}
          </span>
        }) }</span>
      </Typography> : null
    }
  </React.Fragment>
}

export default BasicCharacterInfo