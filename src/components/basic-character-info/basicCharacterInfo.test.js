import { render } from '@testing-library/react'
import BasicCharacterInfo from './basicCharacterInfo'

const props = {
  character: {
    allegiances: [
      'https://www.anapioficeandfire.com/api/houses/23'
    ],
    books: [
      'https://www.anapioficeandfire.com/api/books/1', 
      'https://www.anapioficeandfire.com/api/books/2'
    ],
    born: "In 263 AC or 264 AC",
    died: "299 AC, at Pyke",
    name: "Alannys Harlaw",
  }, 
  books: {
    1: {url: 'https://www.anapioficeandfire.com/api/books/1', name: 'A Game of Thrones'},
    2: {url: 'https://www.anapioficeandfire.com/api/books/2', name: 'A Clash of Kings'},
  },  
  houses: {
    23: {name: "House Blackfyre of King's Landing"}
  }
}

describe('---BasicCharacterInfo Component test---', () => {  
  it('should check correct data fields', () => {
    const { getByTestId } = render(<BasicCharacterInfo {...props} />)
    expect(getByTestId("list-item-name").innerHTML).toEqual("Alannys Harlaw")
    expect(getByTestId("was-born").innerHTML).toEqual("was born In 263 AC or 264 AC")
    expect(getByTestId("died").innerHTML).toEqual("died: 299 AC, at Pyke")
    expect(getByTestId("alliances").innerHTML).toEqual("With alliances to: <span>House Blackfyre of King's Landing</span>")
    expect(getByTestId("meet-in-books").innerHTML).toEqual("Meet in books: <span class=\"book-item false\">A Game of Thrones, </span><span class=\"book-item false\">A Clash of Kings</span>")
  })
})